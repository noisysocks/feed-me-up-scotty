# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

The following changes have been implemented but not released yet:

## [Unreleased]

The following sections document changes that have been released already:

## [1.0.0] - 2021-07-02

### New features

First release! Few customisation options, but feed generations of arbitrary
pages is possible.
